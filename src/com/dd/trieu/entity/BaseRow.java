package com.dd.trieu.entity;

import java.util.ArrayList;
import java.util.stream.Stream;

public abstract class BaseRow implements IDentity {
    protected int id;
    protected String name;

    public BaseRow(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public BaseRow() {

    }
    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId() {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}