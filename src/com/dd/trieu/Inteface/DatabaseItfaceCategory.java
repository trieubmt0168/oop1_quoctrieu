package com.dd.trieu.Inteface;

import com.dd.trieu.entity.Category;

import java.util.ArrayList;

public interface DatabaseItfaceCategory {
    public int insertTable(String name, Category category);

    public ArrayList<Category> selectTable(String name);

    public Integer updateTable(String name, Category oldcategory, Category newcategory);

    public Boolean deleteTable(String name, Category category);

    public void truncateTable(String name);

    public Integer updateTable1(String name, int id, Category category);

}
