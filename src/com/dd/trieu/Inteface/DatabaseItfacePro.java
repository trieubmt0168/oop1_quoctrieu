package com.dd.trieu.Inteface;

import com.dd.trieu.entity.BaseRow;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;

public interface DatabaseItfacePro {


    public int insertTable(String name, BaseRow product);

    public ArrayList<BaseRow> selectTable(String name);

    public Integer updateTable(String name, BaseRow oldproduct, BaseRow newproduct);

    public Boolean deleteTable(String name, BaseRow product);

    public void truncateTable(String name);

    public Integer updateTable1(String name, int id, BaseRow product);

}
