package com.dd.trieu.dao;

import com.dd.trieu.Inteface.DatabaseItfaceCategory;
import com.dd.trieu.entity.Category;

import java.util.ArrayList;

public class DatabaseCategory implements DatabaseItfaceCategory {
    private ArrayList<Category> categoryTable;

    private DatabaseCategory instants;


    public DatabaseCategory() {
        this.categoryTable = new ArrayList<Category>();

    }

    public ArrayList<Category> getCategoryTable() {
        return categoryTable;
    }

    public void setCategoryTable(ArrayList<Category> categoryTable) {
        this.categoryTable = categoryTable;
    }

    public DatabaseCategory getInstants() {
        return instants;
    }

    public void setInstants(DatabaseCategory instants) {
        this.instants = instants;
    }

    @Override
    public String toString() {
        return "Database{" +
                "category=" + categoryTable +
                ", instants=" + instants +
                '}';
    }


    @Override
    public int insertTable(String name, Category category) {
        int i = 0;
        if (name.equals("category")) {
            this.categoryTable.add(category);
            i = 1;
        }
        return i;
    }

    @Override
    public ArrayList<Category> selectTable(String name) {
        if (name.equals("category")) {
            return this.categoryTable;
        }
        return null;
    }

    @Override
    public Integer updateTable(String name, Category oldcategory, Category newcategory) {
        int i = 0;
        ArrayList<Category> objCategory = selectTable("category");


        Integer oldcategorys = objCategory.indexOf(oldcategory);

        if (name.equals("category")) {
            this.categoryTable.set(oldcategorys, newcategory);
            i = 1;
        }
        return i;
    }

    @Override
    public Boolean deleteTable(String name, Category category) {
        if (name.equals("category")) {
            this.categoryTable.remove(category);
        }
        return false;
    }

    @Override
    public void truncateTable(String name) {
        if (name.equals("category")) {
            this.categoryTable.clear();
            System.out.println("TRuncateCategory");
        }
    }

    @Override
    public Integer updateTable1(String name, int id, Category category) {
        int check = 0;
        if (name.equals("category")) {
            ArrayList<Category> categorys = (ArrayList) selectTable("category");
            Category category1 = categorys.stream().filter(categoryss -> id == categoryss.getId()).findAny()
                    .orElse(null);
            Integer index = categorys.indexOf(category1);
            this.categoryTable.set(index, category);
        }
        return check;
    }
}




//
//    @Override
//    public Integer updateTable1(String name, int id, Product product) {
//        int check = 0;
//        if (name.equals("category")) {
//            ArrayList<Product> products = (ArrayList) selectTable("category");
//            Product product1 = products.stream().filter(productss -> id == productss.getId()).findAny()
//                    .orElse(null);
//            Integer index = products.indexOf(product1);
//            this.categoryTable.set(index, product);
//        }
//        return check;
//    }