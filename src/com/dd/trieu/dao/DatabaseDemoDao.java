package com.dd.trieu.dao;

import java.lang.reflect.Array;
import java.util.List;

public interface DatabaseDemoDao {
    public List<Database> getAll();

    public List<Database> insertTableTest(String name, Object row);

    public Array SelectTableTest(Array name);

    public Integer updateTableTest(int name, int row);

    public Boolean deleteTableTest(boolean name, boolean row);

    public void truncateTableTest(String name);
}
