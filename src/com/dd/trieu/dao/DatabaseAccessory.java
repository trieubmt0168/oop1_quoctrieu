package com.dd.trieu.dao;

import com.dd.trieu.Inteface.DatabaseItfaceAcc;
import com.dd.trieu.Inteface.DatabaseItfaceCategory;
import com.dd.trieu.entity.Accessory;
import com.dd.trieu.entity.Category;

import java.util.ArrayList;

public class DatabaseAccessory implements DatabaseItfaceAcc {
    private ArrayList<Accessory> accessoryTable;

    private DatabaseAccessory instants;


    public DatabaseAccessory() {
        this.accessoryTable = new ArrayList<Accessory>();

    }

    public ArrayList<Accessory> getAccessoryTable() {
        return accessoryTable;
    }

    public void setAccessoryTable(ArrayList<Accessory> accessoryTable) {
        this.accessoryTable = accessoryTable;
    }

    public DatabaseAccessory getInstants() {
        return instants;
    }

    public void setInstants(DatabaseAccessory instants) {
        this.instants = instants;
    }

    @Override
    public String toString() {
        return "Database{" +
                "accessory=" + accessoryTable +
                ", instants=" + instants +
                '}';
    }


    @Override
    public int insertTable(String name, Accessory accessory) {
        int i = 0;
        if (name.equals("accessory")) {
            this.accessoryTable.add(accessory);
            i = 1;
        }
        return i;
    }

    @Override
    public ArrayList<Accessory> selectTable(String name) {
        if (name.equals("accessory")) {
            return this.accessoryTable;
        }
        return null;
    }

    @Override
    public Integer updateTable(String name, Accessory oldaccessory, Accessory newaccessory) {
        int i = 0;
        ArrayList<Accessory> objAccessory = selectTable("accessory");


        Integer oldcccessory = objAccessory.indexOf(oldaccessory);

        if (name.equals("accessory")) {
            this.accessoryTable.set(oldcccessory, newaccessory);
            i = 1;
        }
        return i;
    }

    @Override
    public Boolean deleteTable(String name, Accessory accessory) {
        if (name.equals("accessory")) {
            this.accessoryTable.remove(accessory);
        }
        return false;
    }

    @Override
    public void truncateTable(String name) {
        if (name.equals("accessory")) {
            this.accessoryTable.clear();
            System.out.println("TRuncateaccessory");
        }
    }

    @Override
    public Integer updateTable1(String name, int id, Accessory accessory) {
        int check = 0;
        if (name.equals("accessory")) {
            ArrayList<Accessory> category = (ArrayList) selectTable("accessory");
            Accessory accessory1= category.stream().filter(categoryss -> id == categoryss.getId()).findAny()
                    .orElse(null);
            Integer index = category.indexOf(accessory1);
            this.accessoryTable.set(index, accessory);
        }
        return check;
    }
}




//
//    @Override
//    public Integer updateTable1(String name, int id, Product product) {
//        int check = 0;
//        if (name.equals("category")) {
//            ArrayList<Product> products = (ArrayList) selectTable("category");
//            Product product1 = products.stream().filter(productss -> id == productss.getId()).findAny()
//                    .orElse(null);
//            Integer index = products.indexOf(product1);
//            this.categoryTable.set(index, product);
//        }
//        return check;
//    }