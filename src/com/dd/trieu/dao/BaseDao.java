package com.dd.trieu.dao;


import com.dd.trieu.entity.BaseRow;

import java.util.ArrayList;

public abstract class BaseDao implements IDao{

    private static Database database =Database.getInstance();

    protected String tableName ;


    protected Integer insert(BaseRow row1) {
        int i = database.insertTable(tableName, row1);
        return i;
    }


    protected int update(BaseRow oldBaseRow, BaseRow newBaseRow) {
        int i = database.updateTable(tableName, oldBaseRow, newBaseRow);
        return i;
    }

    protected boolean delete(BaseRow baseRow) {
        boolean check = database.deleteTable(tableName, baseRow);
        return check;
    }

    public ArrayList<BaseRow> findAll() {
        ArrayList<BaseRow> category = database.selectTable(tableName);
        return category;
    }

    public BaseRow findById(int id) {
        ArrayList<BaseRow> categorys = database.selectTable(tableName);
        BaseRow category = categorys.stream().filter(category1 -> id == category1.getId()).findAny().orElse(null);
        return category;
    }


    public BaseRow findByName(String name) {
        ArrayList<BaseRow> categorys = database.selectTable(tableName);
        BaseRow category = categorys.stream().filter(category1 -> name == category1.getName()).findAny().orElse(null);
        return category;
    }


}
