package com.dd.trieu.dao;

import com.dd.trieu.entity.BaseRow;

import java.util.ArrayList;

public class Database {
    private ArrayList<BaseRow> productTable;
    private ArrayList<BaseRow> categoryTable;
    private ArrayList<BaseRow> accessoryTable;
    private ArrayList<BaseRow> BaseRowTable;
    private static Database instants;
    private String tableName;

    public Database() {
        this.productTable = new ArrayList<>();
        this.categoryTable = new ArrayList<>();
        this.accessoryTable = new ArrayList<>();
    }

    public static Database getInstance() {
        if (instants == null) {
            synchronized (Database.class) {
                if (instants == null) {
                    instants = new Database();
                }
            }
        }
        return instants;
    }

    @Override
    public String toString() {
        return "Database{" +
                "product=" + productTable +
                ", CategoryTable=" + categoryTable +
                ", accessoryTable=" + accessoryTable +
                ", instants=" + instants +
                '}';
    }

    public ArrayList<BaseRow> BaseRowTable(String name) {
        ArrayList<BaseRow> baseList = new ArrayList<>();
        if (name.equals("product")) {
            baseList = this.productTable;
        }
        if (name.equals("category")) {
            baseList = this.categoryTable;
        }
        if (name.equals("accessory")) {
            baseList = this.accessoryTable;
        }
        return baseList;
    }

    public int insertTable(String name, BaseRow row) {
        int i = 0;
        BaseRowTable(name).add(row);
        i = 1;
        return i;
    }

    public ArrayList<BaseRow> selectTable(String name) {
        return BaseRowTable(name);

    }

    public Integer updateTable(String name, BaseRow oldRow, BaseRow newRow) {
        int i = 0;
        if (name.equals("product")) {
            ArrayList<BaseRow> objProduct = selectTable("product");
            Integer oldProduct = objProduct.indexOf(oldRow);
            BaseRowTable(name).set(oldProduct, newRow);
            i = 1;
        }
        return i;
    }

    public Boolean deleteTable(String name, BaseRow row) {
        boolean check = false;
        check = BaseRowTable(name).remove(row);
        return check;
    }

    public void truncateTable(String name) {
        BaseRowTable(name).clear();
        System.out.println("Da xoa bang " + name);

    }

    public Integer updateTable1(String name, int id, BaseRow newRow) {

        int check = 0;
        ArrayList<BaseRow> listEntity = selectTable(name);
        BaseRow baseRow = listEntity.stream().filter(baseRows -> id == baseRows.getId()).findAny().orElse(null);
        Integer index = listEntity.indexOf(baseRow);
        BaseRowTable(name).set(index, newRow);
        check = 1;
        return check;
    }
}
