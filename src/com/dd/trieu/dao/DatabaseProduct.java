package com.dd.trieu.dao;

import com.dd.trieu.Inteface.DatabaseItfacePro;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;

public class DatabaseProduct implements DatabaseItfacePro {
    private ArrayList<Product> productTable;

    private DatabaseProduct instants;


    public DatabaseProduct() {
        this.productTable = new ArrayList<Product>();

    }

    public ArrayList<Product> getProductTable() {
        return productTable;
    }

    public void setProductTable(ArrayList<Product> productTable) {
        this.productTable = productTable;
    }

    public DatabaseProduct getInstants() {
        return instants;
    }

    public void setInstants(DatabaseProduct instants) {
        this.instants = instants;
    }

    @Override
    public String toString() {
        return "Database{" +
                "product=" + productTable +
                ", instants=" + instants +
                '}';
    }


    @Override
    public int insertTable(String name, Product product) {
        int i = 0;
        if (name.equals("product")) {
            this.productTable.add(product);
            i = 1;
        }
        return i;
    }

    @Override
    public ArrayList<Product> selectTable(String name) {
        if (name.equals("product")) {
            return this.productTable;
        }
        return null;
    }

    @Override
    public Integer updateTable(String name, Product oldproduct, Product newproduct) {
        int i = 0;


        ArrayList<Product> objProduct = selectTable("product");


        Integer oldproducts = objProduct.indexOf(oldproduct);

        if (name.equals("product")) {
            this.productTable.set(oldproducts, newproduct);
            i = 1;
        }
        return i;
    }

    @Override
    public Boolean deleteTable(String name, Product product) {
        if (name.equals("product")) {
            this.productTable.remove(product);
        }
        return false;
    }

    @Override
    public void truncateTable(String name) {
        if (name.equals("product")) {
            this.productTable.clear();
            System.out.println("TRuncateProduct");
        }
    }

    @Override
    public Integer updateTable1(String name, int id, Product product) {
        int check = 0;
        if (name.equals("category")) {
            ArrayList<Product> products = (ArrayList) selectTable("category");
            Product product1 = products.stream().filter(productss -> id == productss.getId()).findAny()
                    .orElse(null);
            Integer index = products.indexOf(product1);
            this.productTable.set(index, product);
        }
        return check;
    }
}
