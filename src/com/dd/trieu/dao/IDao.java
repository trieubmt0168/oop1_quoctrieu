package com.dd.trieu.dao;

import com.dd.trieu.entity.BaseRow;

import java.util.ArrayList;

public interface IDao {

    ArrayList<BaseRow> findAll();

    BaseRow findById(int id);

    BaseRow findByName(String name);


}
