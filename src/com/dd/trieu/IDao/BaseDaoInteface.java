package com.dd.trieu.IDao;


import com.dd.trieu.entity.BaseRow;

import java.util.ArrayList;

public interface BaseDaoInteface {
   public Integer insert(String name, BaseRow accessory);

    public ArrayList<BaseRow> selectTable(String name);

    public Integer update(BaseRow oldBaseRow, BaseRow newBaseRow);

    public   boolean delete(BaseRow baseRow);

    public  ArrayList<BaseRow> findAll();

    public BaseRow findById(int id);

    public BaseRow findByName(String name);

}
